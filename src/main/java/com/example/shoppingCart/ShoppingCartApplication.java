/**
 * Configuration Spring Bean pour Hibernate
 */

package com.example.shoppingCart;

import java.util.Properties;

import javax.activation.DataSource;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

@SpringBootApplication

@EnableAutoConfiguration(exclude = { //  
        DataSourceAutoConfiguration.class, //
        DataSourceTransactionManagerAutoConfiguration.class, //
        HibernateJpaAutoConfiguration.class })
	
public class ShoppingCartApplication {
    @Autowired
    private Environment env;
    
	public static void main(String[] args) {
		SpringApplication.run(ShoppingCartApplication.class, args);
	}
	
	@Bean(name="dataSource")
	public DataSource getDataSource() {
		
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		
		dataSource.setDriverClassName("org.postgresql.Driver");
		dataSource.setUrl("jdbc:postgresql://localhost:5432/panier_jee");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres");
		System.out.println("GetDataSource : " + dataSource);
		
		return (DataSource) dataSource;
	}
	
	@Autowired
	@Bean(name="sessionFactory")
	public SessionFactory getSessionFactory(DataSource dataSource) throws Exception {
		Properties properties = new Properties();
		
		  properties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQL82Dialect");
	        properties.put("hibernate.show_sql", "true");
	        properties.put("current_session_context_class", //
	                "org.springframework.orm.hibernate5.SpringSessionContext");
	        
	        LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
	        
	        factoryBean.setPackagesToScan(new String[] {""});
	        factoryBean.setDataSource((javax.sql.DataSource) dataSource);
	        factoryBean.setHibernateProperties(properties);
	        factoryBean.afterPropertiesSet();
	        
	        SessionFactory sf = factoryBean.getObject();
	        System.out.println("getSessionFactory : " + sf);
	        return sf;
	}
	
	@Autowired
	@Bean(name="transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		
		HibernateTransactionManager TransactionManager = new HibernateTransactionManager(sessionFactory);
		
		return TransactionManager;
	}

}

